## Роль для установки docker server на CentOS/RHEL >= 7.3

Параметры

* docker_server_yum_upgrade - обновить все пакеты yum
* docker_server_install_kernel_4 - установить ядро kernel-lt 4.x
* docker_server_kernel_lt - имя пакета kernel-lt в доступных репозиториях или url rpm-пакета
* docker_server_package - имя пакета docker в доступных репозиториях или url rpm-пакета
* docker_server_lxcfs_package - имя пакета lxcfs в доступных репозиториях или url rpm-пакета
* docker_server_overlay2_daemon_opt - дополнительные параметры записываемые в /etc/docker/daemon.json

При установке ядра kernel-lt 4.x будет выполнена перезагрузка сервера. При использовании штатного ядра включается поддержка overlay2.

### Защита сервера при помощи TLS

Параметры:
* docker_server_tlsverify - настроить доступ к серверу по сети по ssl-ключам
* docker_server_ca_private_key - приватный ключу центра сертификации
* docker_server_ca_public_key - публичный ключу центра сертификации
* docker_server_ca_password - пароль приватного ключа центра сертификации


#### Создание пары ключей центра сертификации

Создание закрытого ключа
```bash
openssl genrsa -aes256 -out ca-key.pem 4096
```

Создание публичного ключа центра сертификации
```bash
openssl req -new -x509 -days 3650 -key ca-key.pem -sha256 -out ca.pem
```

#### Создание клиентского ключа

Адрес сервера задан в переменной `$HOST`

```bash
openssl genrsa -out client-key.pem 4096
openssl req -subj '/CN=client' -new -key client.pem -out client.csr
```

подписываем ключ клиента сертификатом центра сертификации
```bash
echo extendedKeyUsage = clientAuth > extfile.cnf
openssl x509 -req -days 3650 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out client-cert.pem -extfile extfile.cnf
```

Проверяем доступность сервера
```bash
docker --tlsverify --tlscacert=ca.pem --tlscert=client-cert.pem --tlskey=client-key.pem -H=$HOST:2376 version
```

Включить использование tls клиентом по-умолчанию
```bash
mkdir -p ~/.docker
cp -v client-cert.pem ~/.docker/cert.pem
cp -v client-key.pem ~/.docker/key.pem
cp -v ca.pem ~/.docker/ca.pem
export DOCKER_HOST=tcp://$HOST:2376 DOCKER_TLS_VERIFY=1
docker version
```